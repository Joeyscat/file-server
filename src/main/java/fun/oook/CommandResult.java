package fun.oook;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Joey
 * @version 1.0
 * @since 2019/12/20 15:59
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CommandResult {
    private String code;
    private String result;
    private String error;
    private String exception;
}
