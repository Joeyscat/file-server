package fun.oook;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.File;

/**
 * @author Joey
 * @version 1.0
 * @since 2018/10/22 12:27
 */
@Configuration
public class MvcConfig implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // 配置Server虚拟路径，handler为前台访问的目录，location为对应的本地目录
        registry.addResourceHandler("/resources/**").addResourceLocations("file:F:\\game\\");
    }
}
