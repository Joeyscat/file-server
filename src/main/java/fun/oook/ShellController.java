package fun.oook;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Joey
 * @version 1.0
 * @since 2019/12/20 15:21
 */
@RequestMapping("/shell")
@RestController
public class ShellController {

    @PostMapping("/list")
    public ResponseEntity<CommandResult> listFile() {

        String[] cmd = {"cmd.exe /c dir d:\\"};
        CommandResult result = ShellUtils.systemCallWithResult(cmd);
        return ResponseEntity.ok().body(result);
    }


    @PostMapping("/ip")
    public ResponseEntity<CommandResult> ipconfig() {

        String[] cmd = {"ipconfig"};
        CommandResult result = ShellUtils.systemCallWithResult(cmd);
        return ResponseEntity.ok().body(result);
    }

    @PostMapping("/custom")
    public ResponseEntity<CommandResult> custom(@RequestParam("cmd") String cmd) {

        String[] command = {cmd};
        CommandResult result = ShellUtils.systemCallWithResult(cmd);
        return ResponseEntity.ok().body(result);
    }
}
