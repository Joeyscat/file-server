package fun.oook;

import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.connector.ClientAbortException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.Map;

/**
 * AttachmentController
 *
 * @author Joey
 * @version 1.0
 * @since 2019/12/15 12:16
 */
@Slf4j
@Controller
public class AttachmentController {

    private final String path = "D:\\Joey\\书籍\\Java";

    /**
     * https://blog.csdn.net/A1032453509/article/details/78045957
     *
     * @param request  r
     * @param response r
     * @param range    r
     */
    @GetMapping("/range")
    void range(HttpServletRequest request, HttpServletResponse response,
               @RequestParam String filename,
               @RequestHeader(required = false) String range) {
        File file = new File(path, filename);
        long fileLength = file.length();

        log.info("range: " + range);
        Map<String, Object> map = parseRange(range, fileLength);
        long startByte = (long) map.get("start");
        long endByte = (long) map.get("end");

        long contentLength = endByte - startByte + 1;
        String contentType = request.getServletContext().getMimeType(filename);

        response.setHeader("Accept-Ranges", "bytes");
        response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);
        response.setContentType(contentType);
        response.setHeader("Content-Length", String.valueOf(contentLength));
        response.setHeader("Content-Disposition", "attachment;filename=" + filename);
        response.setHeader("Content-Range", "bytes " + startByte + "-" + endByte + "/" + fileLength);

        transfer(file, response, contentLength, startByte, endByte);
    }

    @GetMapping("/file")
    void file(HttpServletRequest request, HttpServletResponse response) {
        File file = new File(path, "");

        long startByte = 0;
        long endByte = file.length() - 1;

        long contentLength = endByte - startByte + 1;
        String filename = file.getName();
        String contentType = request.getServletContext().getMimeType(filename);

        response.setContentType(contentType);
        response.setHeader("Content-Length", String.valueOf(contentLength));
        response.setHeader("Content-Disposition", "attachment;filename=" + filename);

        transfer(file, response, contentLength, startByte, endByte);
    }

    private Map<String, Object> parseRange(String range, long fileLength) {
        // 下载开始位置
        long startByte = 0;
        // 下载结束位置
        long endByte = fileLength - 1;
        if (range != null && range.contains("bytes=") && range.contains("-")) {
            range = range.substring(range.lastIndexOf("=") + 1).trim();
            String[] ranges = range.split("-");
            try {
                // 判断range的类型
                if (ranges.length == 1) {
                    // 类型一：bytes=-2345
                    if (range.startsWith("-")) {
                        endByte = Long.parseLong(ranges[0]);
                    }
                    // 类型二：bytes=2345-
                    else if (range.endsWith("-")) {
                        startByte = Long.parseLong(ranges[0]);
                    }
                }
                // 类型三：bytes=3-2345
                else if (ranges.length == 2) {
                    startByte = Long.parseLong(ranges[0]);
                    endByte = Long.parseLong(ranges[1]);
                }
            } catch (NumberFormatException e) {
                startByte = 0;
                endByte = fileLength - 1;
            }
        }
        Map<String, Object> map = new HashMap<>();
        map.put("start", startByte);
        map.put("end", endByte);
        return map;
    }

    private void transfer(File file, HttpServletResponse response, long contentLength, Long startByte, Long endByte) {
        // 已传输数据大小
        long transmitted = 0;
        try (
                RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r");
                BufferedOutputStream outputStream = new BufferedOutputStream(response.getOutputStream())
        ) {
            byte[] buff = new byte[4096];
            int len = 0;
            randomAccessFile.seek(startByte);
            //
            log.warn("开始下载：" + startByte + "-" + endByte);
            while ((transmitted + len) <= contentLength && (len = randomAccessFile.read(buff)) != -1) {
                outputStream.write(buff, 0, len);
                transmitted += len;
                // 停一下，方便测试，用的时候要删掉
//                Thread.sleep(500);
            }
            // 处理不足buff.length部分
            if (transmitted < contentLength) {
                len = randomAccessFile.read(buff, 0, (int) (contentLength - transmitted));
                outputStream.write(buff, 0, len);
                transmitted += len;
            }

            outputStream.flush();
            response.flushBuffer();
            log.info("下载完毕：" + startByte + "-" + endByte + ":" + transmitted);
        } catch (ClientAbortException cae) {
            log.warn("停止下载：" + startByte + "-" + endByte + ":" + transmitted);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
