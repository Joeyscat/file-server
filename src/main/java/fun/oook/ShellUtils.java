package fun.oook;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Joey
 * @version 1.0
 * @since 2019/12/20 14:03
 */
public class ShellUtils {

    public static int systemCall(String[] cmdArray) {
        Process process;
        int status = -1;
        try {
            process = Runtime.getRuntime().exec(cmdArray);
            status = process.waitFor();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return status;
    }

    public static CommandResult systemCallWithResult(String[] cmdArray) {
        StringBuilder command = new StringBuilder();
        for (String cmd : cmdArray) {
            command.append(cmd).append(" ");
        }
        return systemCallWithResult(command.toString());
    }

    public static CommandResult systemCallWithResult(String command) {
        Process process;
        StringBuilder result = new StringBuilder();
        CommandResult commandResult = new CommandResult();
        try {
            process = Runtime.getRuntime().exec(command);
            String encoding = System.getProperty("sun.jnu.encoding");
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream(), encoding));
            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line).append(" ");
            }
            commandResult.setResult(result.toString());

            BufferedReader errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream(), encoding));
            String error;
            result = new StringBuilder();
            while ((error = errorReader.readLine()) != null) {
                result.append(error).append(" ");
            }
            commandResult.setError(result.toString());

        } catch (IOException e) {
            e.printStackTrace();
            commandResult.setException(e.getLocalizedMessage());
        }
        return commandResult;
    }
}
