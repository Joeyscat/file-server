package fun.oook;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @author Joey
 * @version 1.0
 * @since 2019/12/20 16:22
 */
public class CmdTest {
    public static void main(String[] args) throws IOException {
        ProcessBuilder builder = new ProcessBuilder();
        Runtime runtime = Runtime.getRuntime();
        Process process = runtime.exec("cmd.exe /c dir d:\\");
        InputStream inputStream = process.getInputStream();

        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, "gb2312"));
        String line;
        while ((line = br.readLine()) != null) {
            System.out.println(line);
        }
    }
}
