package fun.oook;


import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@Slf4j
@SpringBootTest
public class ShellUtilsTest {

    @Test
    public void systemCall() {

    }

    @Test
    public void systemCallWithResult() {
        String[] cmd = {"ls"};
        CommandResult commandResult = ShellUtils.systemCallWithResult(cmd);

        log.info(commandResult.toString());
    }
}
